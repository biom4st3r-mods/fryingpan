package com.biom4st3r.fryingpan.mixin;

import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.FallingBlockEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemConvertible;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.World;

import com.biom4st3r.fryingpan.FryingPanBlockEntity;
import com.biom4st3r.fryingpan.ModInit;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(FallingBlockEntity.class)
public abstract class FallingBlockEntityMxn extends Entity {
    
    public FallingBlockEntityMxn(EntityType<?> type, World world) {
        super(type, world);
    }

    @Shadow
    private BlockState block;
    @Shadow 
    public int timeFalling;
    @Shadow 
    public boolean dropItem;
    @Shadow 
    private boolean destroyedOnLanding;
    @Shadow 
    private boolean hurtEntities;
    @Shadow 
    private int fallHurtMax;
    @Shadow 
    private float fallHurtAmount;
    @Shadow 
    public CompoundTag blockEntityData;

    @Override
    public ItemEntity dropItem(ItemConvertible item) {
        BlockEntity be = this.block.getBlock().hasBlockEntity() ? ((BlockEntityProvider)this.block.getBlock()).createBlockEntity(this.world) : null;
        if(be != null && this.blockEntityData != null)
        {
            be.fromTag(block, blockEntityData);
            if(be.getType()==ModInit.BE_FRYING_PAN)
            {
                FryingPanBlockEntity fpbe = (FryingPanBlockEntity) be;
                return fpbe.dropItem(this.world,this.getBlockPos());
            }
        }

        return super.dropItem(item);
    }
}

package com.biom4st3r.fryingpan;

import java.util.Map;
import java.util.UUID;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttribute;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.ToolItem;
import net.minecraft.item.ToolMaterials;
import net.minecraft.item.Wearable;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundCategory;
import net.minecraft.state.property.Properties;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

public class FryingPanItem extends ToolItem implements Wearable {
	Map<EquipmentSlot,ImmutableMultimap<EntityAttribute,EntityAttributeModifier>> attributes = Maps.newHashMap();
	public FryingPanItem(float attackDamage, float attackSpeed,int armor, Settings settings) {
		super(ToolMaterials.IRON, settings);
		ImmutableMultimap.Builder<EntityAttribute, EntityAttributeModifier> builder = ImmutableMultimap.builder();
		
		builder.put(
			EntityAttributes.GENERIC_ATTACK_DAMAGE, 
			new EntityAttributeModifier(
				ATTACK_DAMAGE_MODIFIER_ID,
				"Tool modifier", 
				attackDamage, 
				EntityAttributeModifier.Operation.ADDITION));
		
		builder.put(
			EntityAttributes.GENERIC_ATTACK_SPEED, 
			new EntityAttributeModifier(
				ATTACK_SPEED_MODIFIER_ID,
				"Tool modifier", 
				attackSpeed, 
				EntityAttributeModifier.Operation.ADDITION));

		attributes.put(EquipmentSlot.MAINHAND, builder.build());
		builder = ImmutableMultimap.builder();

		builder.put(
			EntityAttributes.GENERIC_ARMOR, 
			new EntityAttributeModifier(
				UUID.fromString("2AD3F246-FEE1-4E67-B886-69FD380BB150"),
				"Armor modifier", 
				armor, 
				EntityAttributeModifier.Operation.ADDITION));

		attributes.put(EquipmentSlot.HEAD, builder.build());
	}

	@Override
	public boolean isEnchantable(ItemStack stack) {
		return false;
	}

	@Override
	public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
		stack.damage(attacker.world.random.nextInt(3)+1, attacker, (e) -> {
			e.sendEquipmentBreakStatus(EquipmentSlot.MAINHAND);
		});
		return true;
	}

	@Override
	public Multimap<EntityAttribute, EntityAttributeModifier> getAttributeModifiers(EquipmentSlot slot) {
		return attributes.getOrDefault(slot, ImmutableMultimap.of());
	}

	

	@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
		ItemStack stack = user.getStackInHand(hand);
		if(user.getEquippedStack(EquipmentSlot.HEAD).isEmpty())
		{
			user.equipStack(EquipmentSlot.HEAD, stack.copy());
			stack.setCount(0);
			return TypedActionResult.success(stack,world.isClient);
		}
		return TypedActionResult.pass(stack);
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		BlockPos pos = context.getBlockPos();

		if (context.getSide() == Direction.UP && context.getWorld().getBlockState(pos.up()).isAir()) {
			pos = context.getBlockPos().up();
		} else if (context.getSide().ordinal() > 1
				&& context.getWorld().getBlockState(pos.offset(context.getSide())).isAir()
				&& !context.getWorld().getBlockState(pos.offset(context.getSide()).down()).isAir()) {
			pos = context.getBlockPos().offset(context.getSide());
		} else {
			return ActionResult.PASS;
		}
		World world = context.getWorld();
		PlayerEntity player = context.getPlayer();
		context.getWorld().setBlockState(pos, ModInit.BLOCK_FRYING_PAN.getDefaultState()
				.with(Properties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing()));
		this.postPlace(context, context.getWorld().getBlockEntity(pos));

		ModInit.BLOCK_FRYING_PAN.onPlaced(world, pos, ModInit.BLOCK_FRYING_PAN.getDefaultState(), player,
				context.getStack());
		BlockSoundGroup soundGroup = ModInit.BLOCK_FRYING_PAN.getDefaultState().getSoundGroup();
		world.playSound(player, pos, soundGroup.getPlaceSound(), SoundCategory.BLOCKS,
				(soundGroup.getVolume() + 1.0F) / 2.0F, soundGroup.getPitch() * 0.8F);
		if (!player.isCreative())
			context.getStack().decrement(1);
		return ActionResult.SUCCESS;
	}

	private void postPlace(ItemUsageContext context, BlockEntity blockEntity) {
		if(blockEntity.getType() == ModInit.BE_FRYING_PAN)
		{
			((FryingPanBlockEntity)blockEntity).setDamage(context.getStack().getDamage());
		}
		
	}
}
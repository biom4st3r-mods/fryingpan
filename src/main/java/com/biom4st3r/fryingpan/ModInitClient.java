package com.biom4st3r.fryingpan;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.function.Supplier;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.json.JsonUnbakedModel;
import net.minecraft.client.util.ModelIdentifier;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.model.ModelLoadingRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;

/**
 * ModInitClient
 */
public class ModInitClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        
        BlockEntityRendererRegistry.INSTANCE.register(ModInit.BE_FRYING_PAN, (d)->new FryingPanBlockEntityRenderer(d));
        ModelLoadingRegistry.INSTANCE.registerVariantProvider((manager) -> (modelId, context) -> {
            if(modelId.equals(new ModelIdentifier(new Identifier("fryingpan:fryingpan"), "inventory"))) {
                try {
                    return JsonUnbakedModel.deserialize(new InputStreamReader(manager
                            .getResource(new Identifier("fryingpan", "models/fryingpan.json")).getInputStream()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if(modelId.equals(new ModelIdentifier(new Identifier(ModInit.MODID, "burntitem"), "inventory")))
            {
                return new AmbiguouslyBakedModel(){

					@Override
					public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {
                        if(stack.getTag() != null)
                        {
                            BakedModel model = MinecraftClient.getInstance().getItemRenderer().getModels().getModel(Registry.ITEM.get(new Identifier(stack.getTag().getString("hostitem"))));
                            if(model == null) return;
                            context.pushTransform((quad)->
                            {
                                quad.colorIndex(0);
                                int color = 0xFF241a09;
                                quad.spriteColor(0, color, color, color, color);
                                return true;
                            });
                            context.fallbackConsumer().accept(model);
                            context.popTransform();
                        }
					}
                };
            }
            return null;
        });
    }

}
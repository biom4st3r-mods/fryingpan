package com.biom4st3r.fryingpan;

import java.util.Random;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.FallingBlock;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.Waterloggable;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.FallingBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager.Builder;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

import com.google.common.collect.Sets;

import org.jetbrains.annotations.Nullable;

public class FryingPanBlock extends FallingBlock implements Waterloggable, BlockEntityProvider {
    static final VoxelShape SHAPE = Block.createCuboidShape(1, 0, 1, 14, 3, 14);
    static final Set<Block> STICKY = Sets.newHashSet(Blocks.SLIME_BLOCK,Blocks.HONEY_BLOCK);

    public FryingPanBlock(Settings settings) {
        super(settings);
        this.setDefaultState(this.getDefaultState().with(Properties.HORIZONTAL_FACING, Direction.NORTH));
    }

    @Override
    public Item asItem() {
        return ModInit.ITEM_FRYING_PAN;
    }

    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        boolean shouldFall = true;
        for(int i = 0; i < Direction.values().length && shouldFall;i++)
        {
            if(STICKY.contains(world.getBlockState(pos.offset(Direction.values()[i])).getBlock()))
            {
                shouldFall = false;
            }
        }
        if(shouldFall)
        {
            super.scheduledTick(state, world, pos, random);
        }
    }

    @Override
    protected void configureFallingBlockEntity(FallingBlockEntity entity) {
        BlockEntity be = entity.getEntityWorld().getBlockEntity(entity.getBlockPos());
        entity.dropItem = true;

        if(be != null)
        {
            entity.blockEntityData = be.toTag(new CompoundTag());
        }
    }

    @Override
    public void onLanding(World world, BlockPos pos, BlockState fallingBlockState, BlockState currentStateInPos,
            FallingBlockEntity fallingBlockEntity) {
        world.syncWorldEvent(1031, pos, 0);
    }

    @Override
    public void onDestroyedOnLanding(World world, BlockPos pos, FallingBlockEntity fallingBlockEntity) {
        this.onBreak(world, pos, world.getBlockState(pos), null);
    }

    @Override
    public void onBreak(World world, BlockPos pos, BlockState state, @Nullable PlayerEntity player) {

        super.onBreak(world, pos, state, player);
        BlockEntity be = world.getBlockEntity(pos);
        if(be.getType() == ModInit.BE_FRYING_PAN & !world.isClient)
        {
            if(player != null && player.isCreative())
            {
                return;
            }
            FryingPanBlockEntity pan = (FryingPanBlockEntity) be;
            pan.dropItem(world,pos);
        }
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return SHAPE;
    }

    @Override
    public VoxelShape getRaycastShape(BlockState state, BlockView world, BlockPos pos) {
        return SHAPE;
    }

    @Override
    protected void appendProperties(Builder<Block, BlockState> builder) {
        super.appendProperties(builder);
        builder.add(Properties.HORIZONTAL_FACING);
        builder.add(Properties.WATERLOGGED);
    }

    @Override
    public BlockEntity createBlockEntity(BlockView world) {
        return new FryingPanBlockEntity();
    }

}

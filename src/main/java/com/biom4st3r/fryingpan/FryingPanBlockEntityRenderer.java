package com.biom4st3r.fryingpan;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.*;
import net.minecraft.client.render.block.entity.*;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.util.math.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Quaternion;

public class FryingPanBlockEntityRenderer extends BlockEntityRenderer<FryingPanBlockEntity> {

    public FryingPanBlockEntityRenderer(BlockEntityRenderDispatcher dispatcher) {
        super(dispatcher);
    }
    static MinecraftClient client = MinecraftClient.getInstance();
    static RenderLayer textLayer = RenderLayer.getTextSeeThrough(new Identifier(ModInit.MODID, "textures/missing"));
    @Override
    public void render(FryingPanBlockEntity entity, float tickDelta, MatrixStack stack,
            VertexConsumerProvider vertexConsumers, int light, int overlay) {

        if(!entity.currentStack.isEmpty())  
        {
            stack.push();
                stack.scale(0.5F, 0.5F, 0.5F);
                stack.translate(0.5F, 0, 0.5F);
                BakedModel model = client.getItemRenderer().getModels().getModel(entity.currentStack);
                client
                    .getBlockRenderManager()
                    .getModelRenderer()
                    .render(stack.peek(), vertexConsumers.getBuffer(RenderLayer.getCutout()), null, model, 1.0F, 1.0F, 1.0F, light, OverlayTexture.DEFAULT_UV);
            stack.pop();

            final float width = 27;
            float progressionWidth = width * (1F-(entity.remainingTicks/(float)entity.initialTicks));
            progressionWidth += -width/2F;
            stack.push();
                stack.translate(0.5F, 0.5F, 0.5F);
                stack.scale(-0.025F, -0.025F, -0.025F);
                stack.multiply(new Quaternion(Vector3f.NEGATIVE_Y, client.player.getYaw(client.getTickDelta()), true));
                drawRect(0,stack, vertexConsumers.getBuffer(RenderLayer.getSolid()), -width/2F, -2, width/2F, 2, 0xFF000000);
                drawRect(0.1F,stack, vertexConsumers.getBuffer(RenderLayer.getLightning()), -width/2, -2, progressionWidth, 2, 0xFF44FF44);

            stack.pop();        
        }
    }
    private void drawRect(float z, MatrixStack stack,VertexConsumer vc,float minX,float minY,float maxX,float maxY,int color)
    {
        int alpha = ((color & 0xFF000000) >> 24);
        int red = ((color & 0xFF0000) >> 16);
        int green = ((color & 0xFF00) >> 8);
        int blue = ((color & 0xFF));
        vc.vertex(stack.peek().getModel(), minX, maxY, z).color(red, green, blue, alpha).texture(0.09F, 0.10F).light(0xFFFFFFFF).normal(1, 1, 1).next();
        vc.vertex(stack.peek().getModel(), maxX, maxY, z).color(red, green, blue, alpha).texture(0.10F, 0.10F).light(0xFFFFFFFF).normal(1, 1, 1).next();
        vc.vertex(stack.peek().getModel(), maxX, minY, z).color(red, green, blue, alpha).texture(0.10F, 0.09F).light(0xFFFFFFFF).normal(1, 1, 1).next();
        vc.vertex(stack.peek().getModel(), minX, minY, z).color(red, green, blue, alpha).texture(0.09F, 0.09F).light(0xFFFFFFFF).normal(1, 1, 1).next();
    }
}